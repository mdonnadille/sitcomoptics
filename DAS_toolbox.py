import numpy as np
import matplotlib.pyplot as plt
import scipy as sp
from tqdm import tqdm
import datetime


#----------------------------------------------------------------#
"""Signal Processing Functions"""
#----------------------------------------------------------------#

def butterworth_filter(signal, lowcut, highcut, fs, order=3):
    nyquist_freq = 0.5 * fs

    if lowcut is None:
        filter_type = 'low'
        cutoff_freq = highcut / nyquist_freq
    elif highcut is None:
        filter_type = 'high'
        cutoff_freq = lowcut / nyquist_freq
    else:
        filter_type = 'band'
        low         = lowcut / nyquist_freq
        high        = highcut / nyquist_freq
        cutoff_freq = [low, high]

    b, a            = sp.signal.butter(order, cutoff_freq, btype=filter_type)
    filtered_signal = sp.signal.lfilter(b, a, signal)
    return filtered_signal


def seconds_converter(time):
    minutes = time // 60
    seconds = time % 60
    return f"{minutes:02d}:{seconds:02d}"

def normalized(signal, option=1):
    if option == 1:
        # Normalize each row independently
        return np.array([row / np.max(np.abs(row)) for row in signal])
    elif option == 2:
        # Normalize all signals based on the maximum value in the array
        return signal / np.max(np.abs(signal), axis=None)


def fourier_time_domain(time_series, dt):
    N     = len(time_series)
    freqs = np.fft.fftfreq(N, d=dt)
    fft   = np.fft.fft(time_series)
    return freqs, fft

def spectrum(signal, dt):
    if len(signal.shape) == 2:
        if signal.shape[0] == 1:
            signal = signal[0]
        else:
            print("Error: Signal must be a 1D array")
            return None
    N     = len(signal)
    freqs = np.fft.fftfreq(N, d=dt)
    spect = np.abs(np.fft.fft(signal))
    return freqs, spect


def fk_computing(signals, dx, dt):
    #Compute fk domain
    fk_data = np.fft.fft2(signals)
    fk_data = fk_data[:, :fk_data.shape[1]//2]

    num_stations, num_samples = signals.shape
    freqs                     = np.fft.fftfreq(num_samples, dt)[:num_samples//2]
    k                         = np.fft.fftfreq(num_stations, dx)
    #Shift the fk domain
    fk_data = np.fft.fftshift(fk_data, axes=0)
    k       = np.fft.fftshift(k)
    return fk_data, freqs, k

def apply_tukey(signal, alpha=0.2):
    rows, cols   = signal.shape
    tukey_window = sp.signal.windows.tukey(rows, alpha=alpha)[:, np.newaxis] * sp.signal.windows.tukey(cols, alpha=alpha)
    # plt.figure(figsize=(12, 7))
    # plt.rcParams.update({'font.size': 14})
    # plt.imshow(tukey_window, aspect='auto', cmap='gray')
    # # plt.colorbar()
    # plt.show()
    return signal * tukey_window


def apply_gaussian(signal, std_factor=3.75):
    rows, cols = signal.shape
    sigma_row  = rows / std_factor  
    sigma_col  = cols / std_factor  

    # Create 1D Gaussian windows for each dimension
    gauss_window_row = np.exp(-0.5 * ((np.arange(rows) - rows / 2) / sigma_row) ** 2)
    gauss_window_col = np.exp(-0.5 * ((np.arange(cols) - cols / 2) / sigma_col) ** 2)

    # Create a 2D Gaussian window (ellipsoidal) by taking the outer product of the 1D windows
    gaussian_window = np.outer(gauss_window_row, gauss_window_col)
    plt.figure(figsize=(12, 7))
    plt.rcParams.update({'font.size': 14})
    plt.imshow(gaussian_window, aspect='auto', cmap='gray')
    # plt.colorbar()
    plt.show()
    
    return signal * gaussian_window


def stack_data(strain_data_list):
    if not strain_data_list:
        raise ValueError("The list of strain data is empty.")
    shape = strain_data_list[0].shape
    if not all(data.shape == shape for data in strain_data_list):
        raise ValueError("All arrays must have the same shape.")
    stacked_data = np.sum(strain_data_list, axis=0)
    return stacked_data


def strain_to_displacement(strain_data, start_chnl=0, end_chnl=None):
    if end_chnl is None:
        end_chnl = np.shape(strain_data)[0]
    spatial_mean         = np.mean(strain_data, axis=0)
    displacement_data    = (np.cumsum(strain_data - spatial_mean, axis=1)) / (start_chnl - end_chnl)
    return displacement_data

def downsample_time_2D_array(array, original_sampling_rate, target_sampling_rate):
    """
    Downsamples a 2D array along the time axis.

    Parameters:
    - array: A 2D numpy array where the first dimension represents time.
    - original_sampling_rate: The original sampling rate of the array.
    - target_sampling_rate: The desired sampling rate after downsampling.

    Returns:
    - downsampled_array: The downsampled 2D array.
    """
    
    # Calculate the decimation factor, which is the ratio of the original sampling rate to the target sampling rate.
    # Ensure that the decimation factor is an integer.
    decimation_factor = int(original_sampling_rate / target_sampling_rate)
    
    # Downsample the array using the decimation factor.
    # The 'axis=0' parameter ensures that the downsampling occurs along the time axis.
    # 'ftype' is specified as 'iir' to use an IIR filter for anti-aliasing before decimation. This can be adjusted based on requirements.
    downsampled_array = sp.signal.decimate(array, decimation_factor, axis=0, ftype='iir')
    
    return downsampled_array

#----------------------------------------------------------------#
"""Signal Ploting Functions"""
#----------------------------------------------------------------#

def set_time_unit(start_idx, end_idx, dt):
    total_time = (end_idx - start_idx) * dt
    
    if total_time < 3600:  
        return 's'
    elif total_time < 86400:
        return 'm'
    else:  
        return 'h'

def view_raw_data(loaded_data, start_idx=None, end_idx=None, start_chnl=None, end_chnl=None, title=None, dx=None, dt=None, space_tr=10, tick_spacing=1,path_save=None):
    plt.figure(figsize=(25, 14))
    plt.rcParams.update({'font.size': 19})
    if end_chnl is None:
        end_chnl = np.shape(loaded_data)[0]
    if start_chnl is None:
        start_chnl = 0
    if end_idx is None:
        end_idx = np.shape(loaded_data)[1]
    if start_idx is None:
        start_idx = 0
    
    for i in range(start_chnl, end_chnl):
        plt.plot(np.arange(start_idx, end_idx), loaded_data[i, start_idx:end_idx] - space_tr * i, '-k')

    if dt is None:
        plt.xlabel('Time unit')
    else:
        time_unit = set_time_unit(start_idx, end_idx, dt)
        current_ticks = plt.xticks()[0]
        plt.xticks(current_ticks, current_ticks * dt)
        plt.xlabel(f'Time ({time_unit})')

    plt.title(title, fontweight='bold')
    plt.autoscale(enable=True, axis='x', tight=True)
    plt.autoscale(enable=True, axis='y', tight=True)
    if dx is not None:
        y_tick_interval = space_tr * tick_spacing
        y_ticks = -np.arange(start_chnl * space_tr, end_chnl * space_tr, y_tick_interval)
        y_tick_labels = np.arange(start_chnl * dx , end_chnl * dx, dx * tick_spacing)

        plt.yticks(y_ticks, [f'{tick:.0f}' for tick in y_tick_labels])
        plt.ylabel('Distance (m)')
    if path_save is not None:
        plt.savefig(path_save, dpi=300)
    plt.show()
    


def DAScolormap(data, start_dates=None, dx=None, dt=None, title=None, colormap='viridis', vmin=None, vmax=None, strain=True, path_save=None):
    fig, ax = plt.subplots(figsize=(20, 10))
    plt.rcParams.update({'font.size': 19})

    # Create the colormap using absolute values of data
    im = ax.imshow(data, aspect='auto', cmap=colormap, origin='upper', vmin=vmin, vmax=vmax)

    # Set title if provided
    if title:
        ax.set_title(title, fontweight='bold')

    # Setting the x and y labels
    ax.set_ylabel('Channel' if dx is None else 'Distance (m)')

    if dt is not None:
        if start_dates is not None:
            # Corrected format string to match "04h51:00"
            start_time = datetime.datetime.strptime(start_dates, "%Hh%M:%S")
            x_ticks = np.arange(0, data.shape[1], int(data.shape[1] / 10))
            ax.set_xticks(x_ticks)
            ax.set_xticklabels([(start_time + datetime.timedelta(seconds=tick * dt)).strftime('%H:%M:%S') for tick in x_ticks])
            ax.set_xlabel('Time (UTC+1)')
        else:
            # Display time in seconds
            x_ticks = np.arange(0, data.shape[1], int(data.shape[1] / 10))
            ax.set_xticks(x_ticks)
            ax.set_xticklabels([f'{tick * dt:.2f}' for tick in x_ticks])
            ax.set_xlabel('Time (s)')
    else:
        ax.set_xlabel('Time (samples)')

    if dx is not None:
        y_ticks = np.arange(0, data.shape[0], int(data.shape[0] / 10))
        ax.set_yticks(y_ticks)
        ax.set_yticklabels([f'{tick * dx:d}' for tick in y_ticks])

    # Adding a colorbar
    if strain:
        plt.colorbar(im, ax=ax, label='Strain Amplitude')
    else:
        plt.colorbar(im, ax=ax, label='Displacement (m)')

    if path_save is not None:
        plt.savefig(path_save, dpi=300)
    return fig, ax

def plot_spectrum(signal, dt, xlim=None, title=None, path_save=None):
    freqs, spect = spectrum(signal, dt)
    plt.figure(figsize=(21, 13))
    plt.rcParams.update({'font.size': 17})
    plt.plot(freqs, spect, linewidth=2.5, color='k')  
    plt.title(title, fontweight='bold')
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Amplitude')
    if xlim is not None:
        plt.xlim(0, xlim)
    else:
        plt.xlim(0, np.max(freqs[freqs > 0]))
    if path_save is not None:
        plt.savefig(path_save, dpi=300)
    plt.show()



def plot_mean_spectrum(signals, dt, title=None, xlim=None, path_save=None):
    plt.figure(figsize=(21, 13))
    plt.rcParams.update({'font.size': 17})
    spectrums = []
    for signal in tqdm(signals) :
        freqs, spect = spectrum(signal, dt)
        plt.plot(freqs, spect, color='gray', alpha=0.5)
        spectrums.append(spect)
    mean_spect = np.mean(spectrums, axis=0)
    max_val = np.abs(np.max(mean_spect))
    max_freq = np.abs(freqs[np.argmax(mean_spect)])
    plt.plot(freqs, mean_spect, color='r', label='Mean spectrum')
    plt.plot([max_freq, max_freq], [0, max_val], '--', color='r')
    plt.xlim(0, xlim if xlim else np.max(freqs[freqs > 0]))
    plt.ylim(bottom=0)
    plt.title(title, fontweight='bold')
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Amplitude')
    plt.legend()
    plt.text(max_freq, -0.3*max_val, f"{max_freq:.2f} Hz", ha='center', color='r', fontweight='bold')
    if path_save is not None:
        plt.savefig(path_save, dpi=300)
    plt.show()
    return freqs, mean_spect, max_freq, max_val
    
def spectrogram(signal, dt = None, title=None, nfft = None, vmin=None, vmax=None, path_save=None):
    if len(signal.shape) == 2:
        if signal.shape[0] == 1:
            signal = signal[0]
    else:
        print("Error: Signal must be a 1D array")
        return None
    fig, axs = plt.subplots(2, 1, figsize=(16, 12))
    plt.rcParams.update({'font.size': 14})
    fig.suptitle(title, fontsize=16, fontweight='bold', y=1.03)
    axs[0].plot(signal, 'k')
    axs[0].set_ylabel('Amplitude')
    axs[0].set_title('Seismic Trace')
    axs[0].set_xlim(0, len(signal))
    # Check if dt is provided

    if dt is not None:
        fs = int(1 / dt)
        # Generate x-ticks for every second
        total_duration = len(signal) / fs  # This is your signal duration in seconds
        # tick_spacing = total_duration // '...'  # Spacing between ticks in seconds, change it as needed for a different spacing
        # ticks = np.arange(0, total_duration, tick_spacing)
        tick_spacing = 5 * round(total_duration / 5)  # Spacing between ticks in seconds, always a multiple of 5
        num_ticks = np.clip(round(total_duration / tick_spacing), 4, 6)  # Number of ticks, between 4 and 6
        ticks = np.linspace(0, total_duration, num_ticks)

        # Set x-ticks and x-tick labels
        axs[0].set_xticks(ticks * fs)  # Multiply by fs to convert seconds to samples
        axs[0].set_xticklabels([str(int(tick)) for tick in ticks])
        time_unit = set_time_unit(0, len(signal), dt)  # Convert tick locations back to seconds
        axs[0].set_xlabel(f'Time ({time_unit})')
        axs[1].set_xlabel(f'Time ({time_unit})')
    else:
        fs = None
        for ax in axs: 
            ax.set_xlabel('Time unit')
    Pxx, freqs, bins, im = axs[1].specgram(signal, NFFT=nfft, Fs=fs, cmap='jet', vmin=vmin, vmax=vmax)
    cbar = plt.colorbar(im, ax=axs[1])
    cbar.set_label('Intensity')
    axs[1].set_title('Spectrogram')
    axs[1].set_ylabel('Frequency (Hz)')
    plt.tight_layout()
    if path_save is not None:
        plt.savefig(path_save, dpi=300)
    plt.show()

def fk_plot(fk_data, freqs, k, k_normalization=False, title=None, vmin=None, vmax=None, ylim=None, xlim=None, log_scale=False, path_save=None):
    if log_scale:
        fk_data = 20*np.log10(np.abs(fk_data).T + np.finfo(float).eps)
        label_cb = 'Amplitude (db)'
    else:
        fk_data = np.abs(fk_data).T
        label_cb = 'Amplitude'
    
    if k_normalization:
        fk_data = fk_data / np.max(fk_data, axis=0)
        label_cb = 'Amplitude Normalized'
    
    plt.figure(figsize=(16, 12))
    plt.rcParams.update({'font.size': 14})
    plt.imshow(np.abs(fk_data), aspect='auto', 
            extent=[k.min(), k.max(), freqs.max(), freqs.min()],
            cmap='jet',vmin=vmin, vmax=vmax)  # Change colormap to your preference
    plt.title(title, fontweight='bold')
    plt.xlabel('Wavenumber (1/m)')
    plt.ylabel('Frequency (Hz)')
    plt.colorbar(label=label_cb)
    plt.gca().xaxis.set_label_position('top')
    plt.gca().xaxis.set_ticks_position('top')
    if ylim:
        plt.ylim(ylim[::-1])
    if xlim:
        plt.xlim(xlim)
    if path_save is not None:
        plt.savefig(path_save, dpi=300)
    plt.show()





#----------------------------------------------------------------#
#Need rework#
#----------------------------------------------------------------#


def create_transition_matrix(f_range, k_range, v_input, f_resolution=285, k_resolution = 7500, transition_steepness=25):
    # Creating a grid of f and k values
    f_values = np.linspace(f_range[0], f_range[1], f_resolution)
    k_values = np.linspace(k_range[0], k_range[1], k_resolution)
    k_grid, f_grid = np.meshgrid(k_values, f_values)  # Inverting the axes

    # Calculating v and handling division by zero
    with np.errstate(divide='ignore', invalid='ignore'):
        v_grid = np.divide(f_grid, k_grid)
        v_grid[~np.isfinite(v_grid)] = np.nan

    # Normalizing v values within the range [-v_input, v_input]
    normalized_v = (v_grid - (-v_input)) / (2 * v_input)  # Normalize between 0 and 1

    # Applying a sigmoid function for smooth transition
    sigmoid = lambda x: 1 / (1 + np.exp(-x * transition_steepness))
    transition_grid = 1 - (sigmoid(normalized_v) * (1 - sigmoid(normalized_v - 1)))

    # Clipping values to ensure they're within 0-1 range
    transition_grid = np.clip(transition_grid, 0, 1)

    return transition_grid, normalized_v

def apply_transition_to_matrix(matrix, f_range, k_range, v_input, transition_steepness=25):
    # Generate the transition matrix
    transition_matrix, norm = create_transition_matrix(f_range, k_range, v_input=v_input, f_resolution=matrix.shape[0], k_resolution=matrix.shape[1], transition_steepness=transition_steepness)

    # transition_matrix = np.rot90(transition_matrix, k=-1, axes=(1, 0))  

    return np.abs(matrix) * transition_matrix, transition_matrix, np.abs(matrix), norm