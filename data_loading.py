"""
Minimal data loading class for .surfo data files from SNCF DAS.

Author: Martin RUFFEL <ext.martin.ruffel@reseau.sncf.fr>
Copyright © 2023 SNCF DGII.IP3M.SURFO
"""

import os
import pathlib

import numpy as np
import scipy
import time



class SurfoFile:
    """
    Read data from a .surfo file

    Example usage:
    ```python
    sf = SurfoFile("path/to/file.surfo")
    print("Metadata:", sf.metadata)
    print("Data shape:", sf.data.shape)
    ```

    Attributes:
        - path (str): file path
        - metadata (Dict[str, Any]): dictionary containing metadata
        - data (np.array): read-only data array (memory-map to the data stored
            in the file on the disk)
    """

    def __init__(self, path: str):
        self.path = str(pathlib.Path(path).resolve())
        self.metadata = self._read_metadata()
        self.data = np.memmap(filename=self.path,
                              shape=(self.metadata["pulses_count"],
                                     self.metadata["samples_count"]),
                              dtype=np.int8,
                              offset=self.metadata["data_offset"],
                              mode="r")

    def _read_metadata(self):
        """
        Read header from file, along with some complementary information.
        :return: dictionary with metadata information

        :raises ValueError: if the data file is invalid.
        """

        def convert_f(x):
            try:
                y = int(x)
            except ValueError:
                try:
                    y = float(x)
                except ValueError:
                    y = x
            return y

        # Open data file
        with open(self.path, "rb") as file:
            # Extract Header
            # Header size is stored in the first 4 bytes of the file.
            header_size = int(np.fromfile(file, dtype=">u4", count=1)[0])

            # Jump to the beginning of the header
            file.seek(4)

            # Read the complete header
            header = file.read(header_size)

        # Strip trailing zeros
        header = header.rstrip(b'\x00')

        # Decode into a string
        header = header.decode("latin-1")

        # Replace ',' by '.' (to correct float values).
        header = header.replace(",", ".")

        # Split into a list ["key1:value1", "key2:value2" ...]
        header = header.split()

        # Split into a list [("key1", "value1"), ...]
        header = [x.split(":") for x in header]

        # Convert list of pairs into a dictionary.
        header = {x[0]: x[1] for x in header}

        # Try to convert numeric values to int or float.
        header = {k: convert_f(v) for k, v in header.items()}

        # Add complementary info to the header
        # Size of the header
        header["data_offset"] = header_size + 4

        # Refractive index of the fiber
        header["refractive_index"] = 1.468

        # Light speed (in m/s)
        header["light_speed"] = 299_792_458

        if "homodyne" in header["System"].lower():
            raise ValueError("Homodyne data is not supported")

        # Intermediate frequency
        if "intermediateFrequency" not in header:
            header["intermediateFrequency"] = 30e6

        # Data type in which numerical values are encoding
        header["data_encoding"] = "int8"

        # Compute the number of bytes per single data value,
        # based on data_encoding: 'int8' -> 1 byte
        header["bytes_per_value"] = 1

        # Shape of data array
        if "recordCount" in header:
            header["pulses_count"] = header["recordCount"]
            del header["recordCount"]

            header["samples_count"] = header["samplesPerRecord"]
            del header["samplesPerRecord"]
        else:
            # Compute number of pulses by multiplying :
            # - F_repetition (number of pulses in one second)
            # - t_record_reel (number of milliseconds in the sample)
            header["pulses_count"] = int(header["F_repetition"]
                                         * header["t_record_reel"] / 1000)

            # Exact computation of the number of cells.
            # Total number of cells is the number of data values divided by the
            # number of lines (total pulses).
            # Size of the data in bytes.
            nb_bytes = os.path.getsize(self.path) - header["data_offset"]
            nb_values = nb_bytes / header["bytes_per_value"]
            header["samples_count"] = nb_values / header["pulses_count"]

            if int(header["samples_count"]) != header["samples_count"]:
                raise ValueError(
                    f"Total number of cells is not an integer: "
                    f"{header['samples_count']}")

            header["samples_count"] = int(header["samples_count"])

        header["t_record_reel"] /= 1000

        header["channel_spacing"] = (
                header["light_speed"] /
                (2 * header["refractive_index"] * header["F_sampling"])
        )

        header["L_Fibre"] = round(
            header["samples_count"] * header["channel_spacing"]
        )

        header["Resolution"] *= 10

        for key in ["t_execution", "t_record", "P_EDFA", "Pin_AOM",
                    "triggerLevel", "verticalOffset"]:
            if key in header:
                del header[key]

        print(header)
        for old_key, new_key in [
            ("Horaire", "start_time"),
            ("Date", "start_date"),
            ("Resolution", "pulse_width"),
            ("F_sampling", "sampling_frequency"),
            ("F_repetition", "pulse_rate"),
            ("L_Fibre", "fiber_length"),
            ("Vertical_range", "vertical_range"),
            ("t_record_reel", "record_duration"),
            ("System", "system"),
            ("intermediateFrequency", "intermediate_frequency"),
            ("triggerDelay", "trigger_delay"),
        ]:
            if old_key in header:
                header[new_key] = header[old_key]
                del header[old_key]

        return dict(sorted(header.items()))

    def demodulate_signal(self, data, order=3, cutoff="IF",
                          output="A/P"):
        """
        Apply I/Q demodulation on last axis from input data.

        Args:
            data:   input data
            order:  order of the low pass filter
            cutoff: cutoff frequency of the low pass filter
            output: either 'I/Q' or 'A/P'. Sets the return format

        Returns:
            Data after demodulation as either I/Q or Amplitude/Phase pair
            depending on `output` argument.
        """
        start_time = time.time()

        IF = self.metadata["intermediate_frequency"]
        fs = self.metadata["sampling_frequency"]

        if cutoff == "IF" or cutoff is None:
            cutoff = IF

        # Parametrize low pass filter
        low_pass = scipy.signal.butter(order, Wn=cutoff, btype="low",
                                       output="sos",
                                       fs=fs)

        cos = np.cos(2 * np.pi * np.arange(data.shape[-1]) / fs * IF)
        sin = np.sin(2 * np.pi * np.arange(data.shape[-1]) / fs * IF)

        # Demodulate I/Q components
        I = scipy.signal.sosfiltfilt(low_pass, data * cos[..., :],
                                     axis=-1, padtype="constant")
        Q = scipy.signal.sosfiltfilt(low_pass, data * sin[..., :],
                                     axis=-1, padtype="constant")

        if output == "I/Q":
            return I, Q

        elif output == "A/P":

            return np.sqrt(I * I + Q * Q), np.arctan2(Q, I)

        else:
            raise ValueError("Invalid output mode:", output)

    def compute_differential_phase(self, phase, gauge_length_m = 15):
        """
        Compute differential phase.

        Args:
            phase: input phase data
            gauge_length_m: gauge length in meters. This will be rounded to
                a multiple of the channel spacing.

        Returns:
            Differential phase data for the specified gauge_length.
        """
        # Computing Differential Phase
        gauge_length_cells = round(gauge_length_m
                                   / self.metadata["channel_spacing"])
        left  = gauge_length_cells // 2
        right = gauge_length_cells - left

        dp = np.zeros_like(phase)

        dp[:, left:-right] = (
                phase[:, gauge_length_cells:] - phase[:, :-gauge_length_cells]
        )

        # Unwrapping
        return np.unwrap(dp, axis=0)

    def get_strain_scale_factor(
            self,
            gauge_length_m,
            laser_wavelength=1550e-9,
            xi=0.78,  # Lindsey = 0.735, Hubbard = 0.78
            refractive_index=None,
    ):
        """
        Compute strain scale factor.

        Args:
            gauge_length_m: gauge length in meters. This will be rounded to
                a multiple of the channel spacing.
            laser_wavelength: wavelength of the laser
            xi: correction factor that accounts for the strain-optical effect
            refractive_index: refractive index of the fiber. If not specified,
                the value from the metadata is used.

        Returns:
            Strain scale factor
        """
        # Round gauge length to be a multiple of the channel spacing.
        ch_spacing = self.metadata["channel_spacing"]
        gauge_length_m = round(gauge_length_m / ch_spacing) * ch_spacing

        # If no refractive index is specified, use the one in the metadata.
        if refractive_index is None:
            refractive_index = self.metadata["refractive_index"]

        return laser_wavelength / (
                4 * np.pi * xi * refractive_index * gauge_length_m
        )

    def compute_strain_rate_from_differential_phase(self, dp, gauge_length_m,
                                                    **kwargs):
        """
        Convert differential phase to strain rate.

        Args:
            dp: differential phase data
            gauge_length_m: gauge length in meters. This will be rounded to
                a multiple of the channel spacing.
            kwargs: additional parameters for the computation of the scale
                factor. See `get_strain_scale_factor` for more details.

        Returns:
            Strain rate
        """
        strain_scale_factor = self.get_strain_scale_factor(
            gauge_length_m=gauge_length_m,
            **kwargs
        )

        # Differentiate in time to get rate.
        strain_rate = np.diff(dp, axis=0)

        # Apply scale factor and the differentiation factor
        strain_rate *= strain_scale_factor * self.metadata["pulse_rate"]

        return strain_rate


    def downsample_time_2D_array(self, array, original_sampling_rate, target_sampling_rate):
        """
        Downsamples a 2D array along the time axis.

        Parameters:
        - array: A 2D numpy array where the first dimension represents time.
        - original_sampling_rate: The original sampling rate of the array.
        - target_sampling_rate: The desired sampling rate after downsampling.

        Returns:
        - downsampled_array: The downsampled 2D array.
        """

        # Calculate the decimation factor
        decimation_factor = int(original_sampling_rate / target_sampling_rate)

        # Downsample the array 
        downsampled_array = scipy.signal.decimate(array, decimation_factor, axis=0, ftype='iir')

        return downsampled_array